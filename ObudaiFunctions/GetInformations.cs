﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static ObudaiFunctions.Trader;

namespace ObudaiFunctions
{
    static class GetInformations
    {
        /// <summary>
        /// megnézi hogy a szerver elérhető-e
        /// </summary>
        /// <param name="c"></param>
        /// <param name="i"></param>
        /// <param name="lockObject"></param>
        public static async Task<string> GetStatus()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(MyClient.Url);
                client.DefaultRequestHeaders.Add("X-Access-Token", MyClient.SecretKey);
                using (HttpResponseMessage res = await client.GetAsync(client.BaseAddress))
                using (HttpContent content = res.Content)
                {
                    var data = await content.ReadAsStringAsync();
                    Connection data2 = JsonConvert.DeserializeObject<Connection>(data);
                    if (data2.status != null && data2.status == "ok")
                    {
                        return "ok";
                        //foreach (var currency in Enum.GetValues(typeof(CurrencyTypes)))
                        //{
                        //    //ret
                        //    GetCurrencyInfo(currency.ToString(), c, i, lockObject);
                        //}
                    }
                    else
                        return ":(";
                }
            }
        }

        public static async Task<Currency> GetCurrencyInfo(string currency)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(MyClient.Url + "exchange/" + currency);
                client.DefaultRequestHeaders.Add("X-Access-Token", MyClient.SecretKey);
                using (HttpResponseMessage res = await client.GetAsync(client.BaseAddress))
                using (HttpContent content = res.Content)
                {
                    string data = await content.ReadAsStringAsync();
                    Currency data2 = JsonConvert.DeserializeObject<Currency>(data);
                   
                    return data2;

                }
            }
        }

        public static async Task<UserInfo> GetBalance()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(MyClient.Url + "account");
                client.DefaultRequestHeaders.Add("X-Access-Token", MyClient.SecretKey);
                using (HttpResponseMessage res = await client.GetAsync(client.BaseAddress))
                using (HttpContent content = res.Content)
                {
                    string data = await content.ReadAsStringAsync();
                    UserInfo data2 = JsonConvert.DeserializeObject<UserInfo>(data);
                    if (data2 != null)
                    {
                        return data2;
                    }
                }
            }
            return null;
        }
    }
}
