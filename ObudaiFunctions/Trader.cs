using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Build.Framework;
using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace ObudaiFunctions
{
    
    public static class Trader
    {
        static object lockObject = new object();
        public enum CurrencyTypes { btc, eth, xrp };

        [FunctionName("Trader")]
        public static void Run([TimerTrigger("0 */5 * * * *")]TimerInfo myTimer, TraceWriter log)
        {
            log.Info($"C# Timer trigger function executed at: {DateTime.Now}");
            try
            {
                Start(log);
            }
            catch
            {
                log.Info("Kezeletlen 'MEGLEPET�S' t�rt�nt. :(");
            }


        }
        #region MyRegion
        //static async void GetData(Currency[] c, int i, object lockObject)
        //{
        //    using (HttpClient client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri("https://obudai-api.azurewebsites.net/api");
        //        client.DefaultRequestHeaders.Add("X-Access-Token", "033655EF-3DD4-4A6D-9023-08826545760F");
        //        using (HttpResponseMessage res = await client.GetAsync(client.BaseAddress))
        //        using (HttpContent content = res.Content)
        //        {
        //            var data = await content.ReadAsStringAsync();
        //            Connection data2 = await JsonConvert.DeserializeObjectAsync<Connection>(data);
        //            if (data2.status != null && data2.status == "ok")
        //            {
        //                foreach (var currency in Enum.GetValues(typeof(CurrencyTypes)))
        //                {
        //                    GetCurrencyInfo(currency.ToString(), c, i, lockObject);
        //                }
        //            }
        //        }
        //    }
        //}

        //static async void GetCurrencyInfo(string currency, Currency[] c, int i, object lockObject)
        //{
        //    using (HttpClient client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri("https://obudai-api.azurewebsites.net/api/exchange/" + currency);
        //        client.DefaultRequestHeaders.Add("X-Access-Token", "033655EF-3DD4-4A6D-9023-08826545760F");
        //        using (HttpResponseMessage res = await client.GetAsync(client.BaseAddress))
        //        using (HttpContent content = res.Content)
        //        {
        //            string data = await content.ReadAsStringAsync();
        //            Currency data2 = JsonConvert.DeserializeObject<Currency>(data);
        //            if (data2 != null)
        //            {
        //                lock (lockObject)
        //                {
        //                    c[i] = data2;
        //                    i++;
        //                }
        //            }
        //        }
        //    }
        //}

        //static async Task<UserInfo> GetBalance()
        //{
        //    using (HttpClient client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(MyClient.Url + "account");
        //        client.DefaultRequestHeaders.Add("X-Access-Token", MyClient.SecretKey);
        //        using (HttpResponseMessage res = await client.GetAsync(client.BaseAddress))
        //        using (HttpContent content = res.Content)
        //        {
        //            string data = await content.ReadAsStringAsync();
        //            UserInfo data2 = JsonConvert.DeserializeObject<UserInfo>(data);
        //            if (data2 != null)
        //            {
        //                return data2;
        //            }
        //        }
        //    }
        //    return null;
        //} 
        #endregion

        private static async void Start(TraceWriter tw)
        {
            tw.Info($"it begins");
            if (await GetInformations.GetStatus() == "ok")
            {
                UserInfo ui = await GetInformations.GetBalance();
                tw.Info($"{ui.token} Sikeres kapcsol�d�s");
                List<Currency> currList = new List<Currency>();
                foreach (CurrencyTypes currency in Enum.GetValues(typeof(CurrencyTypes)))
                {
                    Currency c = await GetInformations.GetCurrencyInfo(currency.ToString());
                    currList.Add(c);
                    double amount =  ShouldISell(ui, c);
                    if (amount>0)
                    {
                        SellCurrency(currency.ToString(), amount, tw);
                    }
                    
                }
                
                if (ui.usd>0)
                {
                    
                    tw.Info($"T�ll�pett rajta �s megvannak az �rfolyamok");
                    Dictionary<Currency, double> nyeresegek=new Dictionary<Currency, double>();
                    foreach (Currency currency in currList)
                    {
                       nyeresegek.Add(currency, ShouldIBuy(ui, currency,tw));
                    }
                    double maxGain = nyeresegek.Max(x => x.Value * x.Key.currentRate);
                    var maxProfit = nyeresegek.Where(x => x.Value == maxGain / x.Key.currentRate).First();
                    tw.Info("Maxprofit: "+maxProfit.ToString());
                    if (maxGain > 0)
                    {
                        //var max = nyeresegek.Where(x => x.Value == maxProfit.Value).Select(y => y.Key).First();
                        
                        PurchaseCurrency(maxProfit.Key.symbol, nyeresegek[maxProfit.Key],tw);
                        tw.Info($"{maxProfit.Key.symbol}-b�l ker�l v�s�rl�sra {nyeresegek[maxProfit.Key]} mennyis�g");
                    }
                }
                
            }
            else
                tw.Info("Az api nem el�rhet�");
        }

        /// <summary>
        /// Ha megvan hogy mi a legjobb v�laszt�s akkor vesz
        /// </summary>
        /// <param name="type">mib�l</param>
        /// <param name="amount">mennyit</param>
        /// <param name="tw">csak ki�r�shoz kell</param>
        static async void PurchaseCurrency(string type, double amount, TraceWriter tw)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(MyClient.Url+"account/purchase");
                client.DefaultRequestHeaders.Add("X-Access-Token", MyClient.SecretKey);

                CurrencyForTrade currencyForTrade = new CurrencyForTrade() { symbol = type, amount = amount };
                string a= JsonConvert.SerializeObject(currencyForTrade);
                var content = new StringContent(a, Encoding.UTF8, "application/json");

                var result = await client.PostAsync(client.BaseAddress, content);
                //tw.Info("yeeeey"+result.StatusCode);
            }
        }

        static async void SellCurrency(string type, double amount, TraceWriter tw)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(MyClient.Url + "account/sell");
                client.DefaultRequestHeaders.Add("X-Access-Token", MyClient.SecretKey);

                CurrencyForTrade currencyForTrade = new CurrencyForTrade() { symbol = "XRP", amount = 0.000000001 };
                string a = JsonConvert.SerializeObject(currencyForTrade);
                var content = new StringContent(a, Encoding.UTF8, "application/json");

                var result = await client.PostAsync(client.BaseAddress, content);
                //tw.Info("yeeeey"+result.StatusCode);
            }
        }
        /// <summary>
        /// A logika ami megmondja hogy mib�l mennyit lehetne venni ha �rdemesnek tal�lja
        /// </summary>
        /// <param name="ui">a felhaszn�l� az egyenleghez</param>
        /// <param name="currency">a p�nznem</param>
        /// <param name="tw">ki�r�shoz</param>
        /// <returns>mennyit vegyen az adott p�nznemb�l, felt�ve hogy ez �ri meg a legjobban</returns>
        static double ShouldIBuy(UserInfo ui, Currency currency, TraceWriter tw)
        {
            double avg = 0;
            foreach (double price in currency.history.Values)
            {
                avg += price;
            }
            avg = avg / currency.history.Count;
            //tw.Info(currency.currentRate +"\t"+ avg);
            if (currency.currentRate == 0)
            {
                return double.MaxValue;
            }
            else if (currency.history.Take(2).Last().Value - currency.currentRate >= currency.currentRate / (double)10 )
            {
                return ui.usd  / currency.currentRate;
            }
            else if (currency.history.Take(2).Last().Value - currency.currentRate >= currency.currentRate / (double)30 )
            {
                return (ui.usd / 2) / currency.currentRate;
            }
            else if (currency.history.Take(2).Last().Value - currency.currentRate >= currency.currentRate / (double)50)
            {
                return (ui.usd /5)/ currency.currentRate;
            }
            else if (currency.history.Take(2).Last().Value - currency.currentRate >= currency.currentRate / (double)70 )
            {
                return (ui.usd / 7) / currency.currentRate;
            }
            else if (currency.history.Take(2).Last().Value - currency.currentRate >= currency.currentRate / (double)100 )
            {
                return (ui.usd / 10) / currency.currentRate;
            }
            else if (currency.symbol == "XRP")
            {
                return (double)1 / (double)1000;
            }
            else if ( currency.currentRate < avg)
            {
                return (ui.usd/avg) * ( currency.currentRate/avg);
            }
           
            else
                return 0;
        }

        /// <summary>
        /// elad�si logika
        /// </summary>
        /// <param name="ui"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        static double ShouldISell(UserInfo ui, Currency currency)
        {
            double avg = currency.history.Values.Take(5).Average();
            if (currency.currentRate > avg)
            {
                if (currency.currentRate > avg + 0.5 * avg)
                {
                    if (currency.symbol.ToLower()=="btc")
                    {
                        return ui.btc;
                    }
                    if (currency.symbol.ToLower() == "xrp")
                    {
                        return ui.xrp;
                    }
                    if (currency.symbol.ToLower() == "eth")
                    {
                        return ui.eth;
                    }
                }
                else if (currency.currentRate > avg + 0.3 * avg)
                {
                    if (currency.symbol.ToLower() == "btc")
                    {
                        return ui.btc/2;
                    }
                    if (currency.symbol.ToLower() == "xrp")
                    {
                        return ui.xrp/2;
                    }
                    if (currency.symbol.ToLower() == "eth")
                    {
                        return ui.eth/2;
                    }
                }
                else if (currency.currentRate > avg + 0.2 * avg)
                {
                    if (currency.symbol.ToLower() == "btc")
                    {
                        return ui.btc / 2;
                    }
                    if (currency.symbol.ToLower() == "xrp")
                    {
                        return ui.xrp / 2;
                    }
                    if (currency.symbol.ToLower() == "eth")
                    {
                        return ui.eth / 2;
                    }
                }
                else if (currency.currentRate > avg + 0.1 * avg)
                {
                    if (currency.symbol.ToLower() == "btc")
                    {
                        return ui.btc / 4;
                    }
                    if (currency.symbol.ToLower() == "xrp")
                    {
                        return ui.xrp / 4;
                    }
                    if (currency.symbol.ToLower() == "eth")
                    {
                        return ui.eth / 4;
                    }
                }
                else if (currency.currentRate > avg + 0.05 * avg)
                {
                    if (currency.symbol.ToLower() == "btc")
                    {
                        return ui.btc / 10;
                    }
                    if (currency.symbol.ToLower() == "xrp")
                    {
                        return ui.xrp / 10;
                    }
                    if (currency.symbol.ToLower() == "eth")
                    {
                        return ui.eth / 10;
                    }
                }
                return 0;
            }
            else
                return 0;
        }
    }
    

}
