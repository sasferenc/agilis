﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ObudaiFunctions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            GetData();
            Console.ReadKey();
        }

        static async void GetData()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://obudai-api.azurewebsites.net/api");
                client.DefaultRequestHeaders.Add("X-Access-Token", "033655EF-3DD4-4A6D-9023-08826545760F");
                using (HttpResponseMessage res = await client.GetAsync(client.BaseAddress))
                using (HttpContent content = res.Content)
                {
                    var data = await content.ReadAsStringAsync();
                    // dynamic data3 = await JsonConvert.DeserializeObjectAsync(data);
                    // JsonConvert.DeserializeObject(data);
                    Connection data2 = JsonConvert.DeserializeObject<Connection>(data);
                    //var data = await content.ReadAsStringAsync();
                    if (data2.status != null && data2.status == "ok")
                    {
                        Console.WriteLine(data2.status);
                        Console.WriteLine("----------------------------------------------");
                        Console.WriteLine("Eljutottam idáig");
                        Task.Run(() => GetBTCInfo());
                        Console.WriteLine("Túljutottam rajta");
                    }
                }
            }
        }

        static async void GetBTCInfo()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://obudai-api.azurewebsites.net/api/exchange/btc");
                client.DefaultRequestHeaders.Add("X-Access-Token", "033655EF-3DD4-4A6D-9023-08826545760F");
                using (HttpResponseMessage res = await client.GetAsync(client.BaseAddress))
                using (HttpContent content = res.Content)
                {
                    string data = await content.ReadAsStringAsync();
                    Currency data2 = JsonConvert.DeserializeObject<Currency>(data);
                    if (data2 != null)
                    {
                        foreach (var item in data2.history)
                        {
                            Console.WriteLine(item.Value.ToString());
                        }
                    }
                }
            }
        }
    }
}
