﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObudaiFunctions
{
    public class Currency
    {
        public string symbol { get; set; }
        public double currentRate { get; set; }
        public string lastRefreshed { get; set; }
        public string timeZone { get; set; }
        public Dictionary<string, double> history { get; set; }
    }

}
