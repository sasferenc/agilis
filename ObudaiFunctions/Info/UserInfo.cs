﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObudaiFunctions
{
   public class UserInfo
    {
        public string token { get; set; }
        public double usd { get; set; }
        public double btc { get; set; }
        public double eth { get; set; }
        public double xrp { get; set; }
    }

}
